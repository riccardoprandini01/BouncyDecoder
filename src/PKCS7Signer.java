

/**
 *
 * @author es03645
 */
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.encoders.Base64;

public final class PKCS7Signer {

    private String PATH_TO_KEYSTORE = "D:\\TOOL\\keystore.jks";
    private String KEY_ALIAS_IN_KEYSTORE = "chiave bazucco";
    private String KEYSTORE_PASSWORD = "";
    private String SIGNATUREALGO = "SHA1withRSA";
    private String KEY_PASSWORD="";

    public PKCS7Signer(String PATH_TO_KEYSTORE,String KEYSTORE_PASSWORD,String KEY_ALIAS_IN_KEYSTORE,String KEY_PASSWORD) {
        this.PATH_TO_KEYSTORE=PATH_TO_KEYSTORE;
        this.KEYSTORE_PASSWORD=KEYSTORE_PASSWORD;
        this.KEY_ALIAS_IN_KEYSTORE=KEY_ALIAS_IN_KEYSTORE;
        this.KEY_PASSWORD=KEY_PASSWORD;
    }

    KeyStore loadKeyStore() throws Exception {

        KeyStore keystore = KeyStore.getInstance("JKS");
        InputStream is = new FileInputStream(PATH_TO_KEYSTORE);
        keystore.load(is, KEYSTORE_PASSWORD.toCharArray());
        return keystore;
    }

    CMSSignedDataGenerator setUpProvider(final KeyStore keystore) throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        Certificate[] certchain = (Certificate[]) keystore.getCertificateChain(KEY_ALIAS_IN_KEYSTORE);

        final List<Certificate> certlist = new ArrayList<Certificate>();

        for (int i = 0, length = certchain == null ? 0 : certchain.length; i < length; i++) {
            certlist.add(certchain[i]);
        }

        Store certstore = new JcaCertStore(certlist);

        Certificate cert = keystore.getCertificate(KEY_ALIAS_IN_KEYSTORE);

        ContentSigner signer = new JcaContentSignerBuilder(SIGNATUREALGO).setProvider("BC").
                build((PrivateKey) (keystore.getKey(KEY_ALIAS_IN_KEYSTORE, KEY_PASSWORD.toCharArray())));

        CMSSignedDataGenerator generator = new CMSSignedDataGenerator();

        generator.addSignerInfoGenerator(new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().setProvider("BC").
                build()).build(signer, (X509Certificate) cert));

        generator.addCertificates(certstore);

        return generator;
    }

    byte[] signPkcs7(final byte[] content, final CMSSignedDataGenerator generator) throws Exception {

        CMSTypedData cmsdata = new CMSProcessableByteArray(content);
        CMSSignedData signeddata = generator.generate(cmsdata, true);
        return signeddata.getEncoded();
    }

    String doSign(String text) {
        try {
            KeyStore keyStore = this.loadKeyStore();
            CMSSignedDataGenerator signatureGenerator = this.setUpProvider(keyStore);
            byte[] signedBytes = this.signPkcs7(text.getBytes("UTF-8"), signatureGenerator);
            return new String(Base64.encode(signedBytes));
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    
}