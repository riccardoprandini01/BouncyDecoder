
/**
 *
 * @author es03645
 */
import eu.europa.ec.markt.dss.DigestAlgorithm;
import eu.europa.ec.markt.dss.parameter.SignatureParameters;
import eu.europa.ec.markt.dss.signature.DSSDocument;
import eu.europa.ec.markt.dss.signature.InMemoryDocument;
import eu.europa.ec.markt.dss.signature.SignatureLevel;
import eu.europa.ec.markt.dss.signature.SignaturePackaging;
import eu.europa.ec.markt.dss.signature.cades.CAdESService;
import eu.europa.ec.markt.dss.signature.token.DSSPrivateKeyEntry;
import eu.europa.ec.markt.dss.signature.token.JKSSignatureToken;
import eu.europa.ec.markt.dss.signature.token.KSPrivateKeyEntry;
import eu.europa.ec.markt.dss.validation102853.CommonCertificateVerifier;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.security.cert.Certificate;
import java.util.Date;
import org.bouncycastle.util.encoders.Base64;

/**
 * How to sign with CAdES-BASELINE-B enveloping signature.
 */
public final class SelfSignedAttachedEncoder {

    private String PATH_TO_KEYSTORE = "C:\\Users\\Riccardo\\Downloads\\keystore.jks";
    private String KEYSTORE_PASSWORD = "";

    private String KEY_ALIAS_IN_KEYSTORE = "chiave bazucco";
    private String KEY_ALIAS_PSW_IN_KEYSTORE = "";

    private static final String SIGNATUREALGO = "SHA1withRSA";

    SelfSignedAttachedEncoder(String PATH_TO_KEYSTORE, String KEYSTORE_PASSWORD, String KEY_ALIAS_IN_KEYSTORE, String KEY_ALIAS_PSW_IN_KEYSTORE) {
        this.PATH_TO_KEYSTORE = PATH_TO_KEYSTORE;
        this.KEYSTORE_PASSWORD = KEYSTORE_PASSWORD;
        this.KEY_ALIAS_IN_KEYSTORE = KEY_ALIAS_IN_KEYSTORE;
        this.KEY_ALIAS_PSW_IN_KEYSTORE = KEY_ALIAS_PSW_IN_KEYSTORE;
    }

    public String doSign(String msg) {

        try {
            Security.addProvider(new BouncyCastleProvider());

            //----------------------------------------------------    
            //Recupero della chiave dal keystore
            //KSPrivateKeyEntry privateKey = null;
            DSSPrivateKeyEntry privateKey = null;

            URL URL_TO_KEYSTOREFile = new File(PATH_TO_KEYSTORE).toURI().toURL();
            //Ottengo la chiave con parte pubblica privata e chain conforme a DSSPrivateKeyEntry
            privateKey = this.getPrivateKey(URL_TO_KEYSTOREFile, KEYSTORE_PASSWORD, KEY_ALIAS_IN_KEYSTORE, KEY_ALIAS_PSW_IN_KEYSTORE);

            //----------------------------------------------------    
            // Preparing parameters for the CAdES signature
            SignatureParameters parameters = new SignatureParameters();
            // We choose the level of the signature (-B, -T, -LT, -LTA).
            parameters.setSignatureLevel(SignatureLevel.CAdES_BASELINE_B);
            // We choose the type of the signature packaging (ENVELOPING, DETACHED).
            parameters.setSignaturePackaging(SignaturePackaging.ENVELOPING);
            // We set the digest algorithm to use with the signature algorithm. You must use the
            // same parameter when you invoke the method sign on the token. The default value is
            // SHA256
            parameters.setDigestAlgorithm(DigestAlgorithm.SHA256);
            // We choose the private key with the certificate and corresponding certificate
            // chain.
            parameters.setPrivateKeyEntry(privateKey);
            //
            //parameters.setSigningCertificate(privateKey.getCertificate());
            //parameters.setCertificateChain(privateKey.getCertificateChain());
            parameters.bLevel().setSigningDate(new Date());

            //----------------------------------------------------------------------------
            // Create common certificate verifier
            CommonCertificateVerifier commonCertificateVerifier = new CommonCertificateVerifier();
            // Create CAdES Service for signature
            CAdESService service = new CAdESService(commonCertificateVerifier);
        //XAdESService service = new XAdESService(commonCertificateVerifier);

            //DSSDocument toSignDocument = new FileDocument("C:\\readme.txt");
            //FileDocument toSignDocument = new FileDocument("C:\\readme.txt");
            //CommonDocument toSignDocument = new CommonDocument(toSignFilePath);
            //DigestDocument toSignDocument = new DigestDocument(toSignFilePath);
            byte[] bytes = msg.getBytes(Charset.forName("UTF-8"));
            InMemoryDocument toSignDocument = new InMemoryDocument(bytes);

            //----------------------------------------------------------------------------
            // Get the SignedInfo that need to be signed.
            byte[] dataToSign = service.getDataToSign(toSignDocument, parameters);
        //this.showB64(dataToSign);

            // This function obtains the signature value for signed information using the
            // private key and specified algorithm
            JKSSignatureToken signingToken = null;

            signingToken = new JKSSignatureToken(new File(PATH_TO_KEYSTORE).toURI().toURL().toString(), KEYSTORE_PASSWORD);

            byte[] signatureValue = signingToken.sign(dataToSign, parameters.getDigestAlgorithm(), privateKey);
        //this.showB64(signatureValue);

            /*
             File f= new File("C:\\Users\\Riccardo\\output.b64");
             FileOutputStream fos=null;
             try {
             fos= new FileOutputStream(f);
             } catch (FileNotFoundException ex) {
             Logger.getLogger(SelfSignedAttachedEncoder.class.getName()).log(Level.SEVERE, null, ex);
             }
             */
            // We invoke the service to sign the document with the signature value obtained in
            // the previous step.
            DSSDocument signedDocument = service.signDocument(toSignDocument, parameters, signatureValue);
            /*
             DSSUtils.copy(signedDocument.openStream(), fos);
             */
            byte[] bb = signedDocument.getBytes();
            return new String(Base64.encode(bb));

        } catch (MalformedURLException e) {
            return e.getMessage();
        } catch (KeyStoreException e) {
           return e.getMessage();
        } catch (IOException e) {
            return e.getMessage();
        } catch (NoSuchAlgorithmException e) {
           return e.getMessage();
        } catch (UnrecoverableKeyException e) {
            return e.getMessage();
        } catch (CertificateException e) {
            return e.getMessage();
        }

    }

    public KSPrivateKeyEntry getPrivateKey(URL ksLocation, String keyStorePassword, String certAlias, String certPassword) throws KeyStoreException, IOException, NoSuchAlgorithmException, UnrecoverableKeyException, CertificateException {
       
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream ksStream = ksLocation.openStream();
            keyStore.load(ksStream, keyStorePassword.toCharArray());

            final Key key = keyStore.getKey(certAlias, certPassword.toCharArray());
            if (key == null) {
                return null;
            }
            if (!(key instanceof PrivateKey)) {
                return null;
            }

            Certificate[] certificateChain = keyStore.getCertificateChain(certAlias);
            KeyStore.PrivateKeyEntry privateKey = new KeyStore.PrivateKeyEntry((PrivateKey) key, certificateChain);
            KSPrivateKeyEntry ksPrivateKey = new KSPrivateKeyEntry(privateKey);
            return ksPrivateKey;
      
        

    }

    private void showB64(byte[] dataToSign) {
        byte[] encodedBytes1 = Base64.encode(dataToSign);

        String str = null;
        try {
            str = new String(encodedBytes1, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SelfSignedAttachedEncoder.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(str);
    }
}
