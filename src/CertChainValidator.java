/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author es03645
 */

import java.io.ByteArrayInputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;

public class CertChainValidator {

    /**
     * Validate keychain
     *
     * @param client is the client X509Certificate
     * @param keyStore containing all trusted certificate
     * @return true if validation until root certificate success, false
     * otherwise
     * @throws KeyStoreException
     * @throws CertificateException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static boolean validateKeyChain(X509Certificate client,
            KeyStore keyStore) throws KeyStoreException, CertificateException,
            InvalidAlgorithmParameterException, NoSuchAlgorithmException,
            NoSuchProviderException {
        X509Certificate[] certs = new X509Certificate[keyStore.size()];
        int i = 0;
        Enumeration<String> alias = keyStore.aliases();

        while (alias.hasMoreElements()) {
            certs[i++] = (X509Certificate) keyStore.getCertificate(alias
                    .nextElement());
        }

        //il tipo di certificato deve essere omogeneo per classe di implementazione altrimenti l'hash non torna
        java.security.cert.X509Certificate x509CertificateC;
        x509CertificateC = null;
        try {
            byte[] encoded = client.getEncoded();
            ByteArrayInputStream bis = new ByteArrayInputStream(encoded);
            java.security.cert.CertificateFactory cf = java.security.cert.CertificateFactory.getInstance("X.509");
            x509CertificateC = (java.security.cert.X509Certificate) cf.generateCertificate(bis);
        } catch (java.security.cert.CertificateEncodingException e) {
        } catch (java.security.cert.CertificateException e) {
        }

        return validateKeyChain(x509CertificateC, certs);
    }

    /**
     * Validate keychain recursively
     *
     * @param client is the client X509Certificate
     * @param trustedCerts is Array containing all trusted X509Certificate
     * @return true if validation until root certificate success, false
     * otherwise
     * @throws CertificateException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static boolean validateKeyChain(X509Certificate client,
            X509Certificate[] trustedCerts) throws CertificateException,
            InvalidAlgorithmParameterException, NoSuchAlgorithmException,
            NoSuchProviderException {
        boolean found = false;
        int i = trustedCerts.length;

        TrustAnchor anchor;
        Set anchors;
        CertPath path;
        List list;
        PKIXParameters params;

        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        CertPathValidator validator = CertPathValidator.getInstance("PKIX");

        while (!found && i > 0) {
            //Come ancora seleziono uno dei certificati della lista
            anchor = new TrustAnchor(trustedCerts[--i], null);
            anchors = Collections.singleton(anchor);

            //Creo il path dal certificato oggetto
            list = Arrays.asList(new Certificate[]{client});
            path = cf.generateCertPath(list);

            //l'ancora è il settaggio del validatore
            params = new PKIXParameters(anchors);
            //Nessun controllo CRL
            params.setRevocationEnabled(false);

            //Se l'issuer del certificato in oggetto è lo stesso che attualmente
            //è preso dalla lista
        
           
               if (client.getIssuerDN().equals(trustedCerts[i].getSubjectDN())) {
                try {
                    //Viene applicata la validazione che può dare eccezione
                    validator.validate(path, params);
                    //Se il certificato dell'issuer è self signed è un CA è ho finito
                    if (SignRelationshipHelper.isSelfSigned(trustedCerts[i])) {
                        // found root ca
                        found = true;
                        System.out.println("validating root" + trustedCerts[i].getSubjectX500Principal().getName());
                    //Altrimenti se sto confrontando il certificato con se stesso
                        //risalgo e confronto questo certificato con la lista
                    } else if (!client.equals(trustedCerts[i])) {
                        // find parent ca
                        System.out.println("validating via:" + trustedCerts[i].getSubjectX500Principal().getName());
                        found = validateKeyChain(trustedCerts[i], trustedCerts);
                    }

                } catch (CertPathValidatorException e) {
                    // validation fail, check next certifiacet in the trustedCerts array
                }
            }
        }

        return found;
    }

   
}
