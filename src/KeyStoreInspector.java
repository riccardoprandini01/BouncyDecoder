
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.x500.X500Principal;
import org.apache.commons.codec.binary.Hex;
import sun.security.rsa.RSAPrivateCrtKeyImpl;
import sun.security.x509.X509CertImpl;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author es03645
 */
public class KeyStoreInspector {

    private String KEYSTOREPATH="";
    private String KEYSTOREPASSWORD="";
    private HashMap aliasPassword;
    private FileInputStream is;

    public KeyStoreInspector(String KEYSTOREPATH, String KEYSTOREPASSWORD, HashMap aliasPassword) {
        this.KEYSTOREPATH = KEYSTOREPATH;
        this.KEYSTOREPASSWORD = KEYSTOREPASSWORD;
        this.aliasPassword = aliasPassword;

    }

    public StringBuilder Inspect() {

        StringBuilder sb = new StringBuilder();

        try {
            KeyStore keystore = this.getKeystore();

            //recupero tutti gli alias del key
            Enumeration enumeration = keystore.aliases();
            //keystore.

            HashMap<String, String> pks = new HashMap();

            //Elenco tutti gli elementi 
            while (enumeration.hasMoreElements()) {
                sb.append("============================================================================").append("\n");
                sb.append("============================================================================").append("\n");
                sb.append("============================================================================").append("\n");
                sb.append("============================================================================").append("\n");

                String alias = (String) enumeration.nextElement();
                sb.append("-Alias name:             ").append(alias).append("\n");
                sb.append("--isKeyEntry:            ").append(keystore.isKeyEntry(alias)).append("\n");
                sb.append("--isCertificateEntry:    ").append(keystore.isCertificateEntry(alias)).append("\n");

                sb.append("--Retriving KEY").append("\n");
                if (this.aliasPassword.containsKey(alias)) {
                    try {
                        Key pk = keystore.getKey(alias, ((String) this.aliasPassword.get(alias)).toCharArray());
                        sb.append("---KEY Detail").append("\n");

                        sb.append("----Algoritm:").append(pk.getAlgorithm()).append("\n");
                        sb.append("----Format:  ").append(pk.getFormat()).append("\n");
                        sb.append("----Encoded: ").append(Hex.encodeHexString(pk.getEncoded())).append("\n");
                        sb.append("----PK:      ").append(pk.toString()).append("\n");
                        if (pk instanceof RSAPrivateCrtKeyImpl) {
                            sb.append("----Algorithm:").append(((RSAPrivateCrtKeyImpl) pk).getAlgorithm()).append("\n");
                            sb.append("----CrtCoeff :").append(((RSAPrivateCrtKeyImpl) pk).getCrtCoefficient()).append("\n");
                            sb.append("----Modulus  :").append(((RSAPrivateCrtKeyImpl) pk).getModulus()).append("\n");
                            sb.append("----PrimExpP :").append(((RSAPrivateCrtKeyImpl) pk).getPrimeExponentP()).append("\n");
                            sb.append("----PrimeExpQ:").append(((RSAPrivateCrtKeyImpl) pk).getPrimeExponentQ()).append("\n");
                            sb.append("----PrimeP   :").append(((RSAPrivateCrtKeyImpl) pk).getPrimeP()).append("\n");
                            sb.append("----PrimeQ   :").append(((RSAPrivateCrtKeyImpl) pk).getPrimeQ()).append("\n");
                            sb.append("----PrivExp  :").append(((RSAPrivateCrtKeyImpl) pk).getPrivateExponent()).append("\n");
                            sb.append("----PublicExp:").append(((RSAPrivateCrtKeyImpl) pk).getPublicExponent()).append("\n");
                        } else {
                            sb.append("----PK:      ").append(pk.toString()).append("\n");
                        }
                        sb.append("");
                    } catch (UnrecoverableKeyException ex) {
                        sb.append("  ").append(ex.getMessage()).append("\n");
                    }
                } else {
                    sb.append("  No password for this alias: ").append(alias).append("\n");
                }

                //Recupero del certificato
                sb.append("--Retriving CERTIFICATE").append("\n");
                Certificate[] chain = keystore.getCertificateChain(alias);
                if (chain != null) {
                    sb.append("--Chain").append("\n");
                    for (Certificate certificate1 : chain) {
                        this.printCertificateDetail(alias, certificate1, sb, keystore);
                    }

                } else {
                    sb.append("--Nochain").append("\n");
                    //Provo ad estrarre il singolo certificato
                    Certificate certificate = keystore.getCertificate(alias);
                    this.printCertificateDetail(alias, certificate, sb, keystore);
                }

            }

            //keystore.getCertificateChain(alias);
            //keystore.getCreationDate(alias);
            sb.append("=============OK END================").append("\n");
        } catch (FileNotFoundException e) {
            sb.append("=============ERROR================").append("\n");
            sb.append(e.toString()).append("\n");
        } catch (KeyStoreException e) {
            sb.append("=============ERROR================").append("\n");
            sb.append(e.toString()).append("\n");
        } catch (NoSuchAlgorithmException e) {
            sb.append("=============ERROR================").append("\n");
            sb.append(e.toString()).append("\n");
        } catch (CertificateException e) {
            sb.append("=============ERROR================").append("\n");
            sb.append(e.toString()).append("\n");

        } catch (IOException e) {
            sb.append("=============ERROR================").append("\n");
            sb.append(e.toString()).append("\n");
        } finally {
            sb.append("=============STEP END================").append("\n");
            this.releaseKeystore();
        }
        return sb;

    }

    private void printCertificateDetail(String alias, Certificate certificate1, StringBuilder sb, KeyStore keystore) {
        if (certificate1 instanceof X509CertImpl) {
            sb.append("---CERTIFICATE:         ").append("\n");
            sb.append("---Type:         ").append(((X509CertImpl) certificate1).getType()).append("\n");
            sb.append("---Version:      ").append(((X509CertImpl) certificate1).getVersion()).append("\n");
            sb.append("---Serial Number:").append("0x").append(((X509CertImpl) certificate1).getSerialNumber().toString(16).toUpperCase()).append("\n");
            sb.append("---From:         ").append(((X509CertImpl) certificate1).getNotBefore()).append("\n");
            sb.append("---To:           ").append(((X509CertImpl) certificate1).getNotAfter()).append("\n");
            boolean isDateValid;
            try {
                ((X509CertImpl) certificate1).checkValidity();
                isDateValid = true;
            } catch (Exception e) {
                isDateValid = false;
            }
            sb.append("---Date OK:      ").append(isDateValid).append("\n");

            X500Principal subjectX500Principal = ((X509CertImpl) certificate1).getSubjectX500Principal();
            X500Principal issuerX500Principal = ((X509CertImpl) certificate1).getIssuerX500Principal();

            sb.append("---Principal:    ").append(subjectX500Principal).append("\n");
            sb.append("---Issuer:       ").append(issuerX500Principal).append("\n");
            sb.append("---SELF SIGN VERIFICATION:         ").append("\n");
            //Verifico se il certivficato può essere validato con la propria PK
            verifyCertPk(certificate1, subjectX500Principal.getName(), certificate1, sb);
            sb.append("---OTHER SIGN VERIFICATION:         ").append("\n");
            //Verifico con le altre chiavi
            this.verifyVSOthers(certificate1, keystore, sb);
        } else {
            sb.append(certificate1.toString()).append("\n");
            sb.append(certificate1.getClass()).append("\n");
        }
    }

    private void verifyVSOthers(Certificate certificate, KeyStore keystore, StringBuilder sb) {
        //verifico il certificato nei confronti delle altre chiavi pubbliche
        Enumeration enumeration2 = null;
        try {
            enumeration2 = keystore.aliases();

            while (enumeration2.hasMoreElements()) {
                String alias2 = (String) enumeration2.nextElement();
                Certificate certI = keystore.getCertificate(alias2);
                this.verifyCertPk(certificate, alias2/*+"-"+((X509CertImpl)certI).getSubjectX500Principal().getName()*/, certI, sb);
            }
        } catch (KeyStoreException ex) {
            Logger.getLogger(KeyStoreInspector.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void verifyCertPk(Certificate certificate, String AliasPK, Certificate certificatePK, StringBuilder sb) {

        boolean thisVerified;
        String thisVerifiedDetail = "";
        try {
            ((X509CertImpl) certificate).verify(certificatePK.getPublicKey());
            thisVerified = true;
        } catch (Exception e) {
            thisVerified = false;
            thisVerifiedDetail = e.getMessage();
        }
        sb.append("----Alias name:          ").append(AliasPK).append("\n");
        sb.append("-----SignedVerify:       ").append(thisVerified).append("\n");
        if (!thisVerified) {
            sb.append("-----SignedVerifyDetail: ").append(thisVerifiedDetail).append("\n");
        }
    }

    ArrayList<String> listKeys()  {
        ArrayList<String> list = new ArrayList<>();
        try {

            KeyStore keystore = this.getKeystore();
            Enumeration enumeration = keystore.aliases();
            while (enumeration.hasMoreElements()) {
                String alias = (String) enumeration.nextElement();
                
                if(keystore.isKeyEntry(alias)){
                    list.add(alias);
                }
            
                /*
                if(keystore.isCertificateEntry(alias)){
                    list.add(alias +("C"));
                } 
                */
                
                
               
            }
        } catch (KeyStoreException ex) {

        } catch (IOException ex) {

        } catch (NoSuchAlgorithmException ex) {

        } catch (CertificateException ex) {

        }finally{
            return list;
        }
    }

    private KeyStore getKeystore() throws KeyStoreException, FileNotFoundException, IOException, NoSuchAlgorithmException, CertificateException {
        File file = new File(KEYSTOREPATH);
        is = new FileInputStream(file);

        //Riferimento al keystore
        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        keystore.load(is, KEYSTOREPASSWORD.toCharArray());

        return keystore;
    }

    private void releaseKeystore() {
        try {
            if (is != null) {
                is.close();
            }
        } catch (IOException ex) {

        }
    }

}
