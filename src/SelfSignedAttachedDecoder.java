
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1UTCTime;
import org.bouncycastle.asn1.DERUTCTime;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.CollectionStore;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.encoders.Base64;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author es03645
 */
public class SelfSignedAttachedDecoder {

    private String KEYSTOREPATH = "";
    private String KEYSTOREPASSWORD = "";

    SelfSignedAttachedDecoder(String KEYSTOREPATH, String KEYSTOREPASSWORD) {
        this.KEYSTOREPATH = KEYSTOREPATH;
        this.KEYSTOREPASSWORD = KEYSTOREPASSWORD;
    }

    String decode(String envelopedData) {

        String s = null;
        try {

            try {
                Class<?> cBC = Class.forName("org.bouncycastle.jce.provider.BouncyCastleProvider");
                java.security.Security.insertProviderAt((java.security.Provider) cBC.newInstance(), 2000);
                cBC.newInstance();
            } catch (Exception e) {
                //JOptionPane.showMessageDialog(rootPane, e.getMessage(), "Error loading BC", JOptionPane.ERROR_MESSAGE);
            }
            //Security.addProvider(new BouncyCastleProvider());
            CMSSignedData cms = null;
            try {
                cms = new CMSSignedData(Base64.decode(envelopedData.getBytes()));
            } catch (Exception e) {
                //JOptionPane.showMessageDialog(rootPane, e.getMessage(), "Error loading BC", JOptionPane.ERROR_MESSAGE);
            }
            if (cms != null) {
                //I certificati alla rinfusa
                Store store = (CollectionStore) cms.getCertificates();
                //this.inspectAllCert(store);
                LinkedList<LinkedList<X509Certificate>> orderedCert = this.orderAllCert(store);
//------------------------------------------------------------------------------                
                System.out.println("Sono presenti i seguenti certificati");
                for (LinkedList<X509Certificate> linkedList : orderedCert) {
                    for (X509Certificate x509Certificate : linkedList) {
                        detailCertificate(x509Certificate);
                    }
                }

//------------------------------------------------------------------------------                
                //Verifica sul keystore
                if (KEYSTOREPATH != null && KEYSTOREPASSWORD != null && KEYSTOREPATH.length() > 0 && KEYSTOREPASSWORD.length() > 0) {
                    System.out.println("******************************************************");
                    //Controllo se il certificato è nel keystore 

                    String storename = KEYSTOREPATH;
                    char[] storepass = KEYSTOREPASSWORD.toCharArray();

                    KeyStore ks = null;

                    ks = KeyStore.getInstance("JKS");

                    FileInputStream fin = null;

                    fin = new FileInputStream(storename);
                    ks.load(fin, storepass);

                    for (LinkedList<X509Certificate> linkedList : orderedCert) {
                        for (X509Certificate x509Certificate : linkedList) {
                            detailCertificate(x509Certificate);

                            if (CertChainValidator.validateKeyChain(x509Certificate, ks)) {
                                System.out.println(">>>>>>>>>>>OK");
                            } else {
                                System.out.println(">>>>>>>>>>>KO");
                            }
                        }
                        System.out.println(" ");
                    }
                }
                System.out.println("******************************************************");

//------------------------------------------------------------------------------                               
                System.out.println("******************************************************");

                for (LinkedList<X509Certificate> linkedList : orderedCert) {
                    for (X509Certificate x509Certificate : linkedList) {
                        detailCertificate(x509Certificate);
                        boolean revoked = false;

                        try {

                            java.security.cert.X509Certificate x509CertificateC;
                            x509CertificateC = null;
                            try {
                                byte[] encoded = x509Certificate.getEncoded();
                                ByteArrayInputStream bis = new ByteArrayInputStream(encoded);
                                java.security.cert.CertificateFactory cf = java.security.cert.CertificateFactory.getInstance("X.509");
                                x509CertificateC = (java.security.cert.X509Certificate) cf.generateCertificate(bis);
                            } catch (java.security.cert.CertificateEncodingException e) {
                            } catch (java.security.cert.CertificateException e) {
                            }

                            CRLVerifier.verifyCertificateCRLs(x509CertificateC);

                            /*if (CertChainValidator.validateKeyChain(x509Certificate, ks)) {
                             System.out.println(">>>>>>>>>>>OK");
                             } else {
                             System.out.println(">>>>>>>>>>>KO");
                             }*/
                        } catch (CertificateVerificationException ex) {
                            revoked= true;
                            System.out.println(ex.toString());
                        }
                        if(revoked){
                            System.out.println("Certificato REVOCATO"); 
                        }else{
                            System.out.println("Certificato VALIDO"); 
                        }
                       
                    }
                    
                    
                    
                }
                System.out.println("******************************************************");

//------------------------------------------------------------------------------                               
                //Il firmatario
                SignerInformationStore signers = cms.getSignerInfos();
                Collection c = signers.getSigners();
                Iterator it = c.iterator();
                while (it.hasNext()) {
                    System.out.println(">>>>>Firmatario");
                    SignerInformation signer = (SignerInformation) it.next();
                    this.getSigninTime(signer);
                    System.out.println(">>>>>Certificati del firmatario");
                    Collection certCollection = store.getMatches(signer.getSID());
                    Iterator certIt = certCollection.iterator();
                    while (certIt.hasNext()) {

                        X509CertificateHolder certHolder = (X509CertificateHolder) certIt.next();
                        X509Certificate cert = new JcaX509CertificateConverter().setProvider("BC").getCertificate(certHolder);

                        detailCertificate(cert);

                        //if (signer.verify(cert.getPublicKey(), "BC"))
                        if (signer.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(cert))) {
                            System.out.println("Messaggio verificato  dal presente firmatario");
                        }

                    }
                }
                byte[] data = (byte[]) cms.getSignedContent().getContent();
                s = new String(data);

            }

        } catch (CMSException ex) {
            Logger.getLogger(BouncyFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(BouncyFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OperatorCreationException ex) {
            Logger.getLogger(BouncyFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(BouncyFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyStoreException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidAlgorithmParameterException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return s;

    }

    private void getSigninTime(SignerInformation signer) throws ParseException {
        Attribute signinTime = signer.getSignedAttributes().get(CMSAttributes.signingTime);

        if (signinTime != null) {
            ASN1Set set = signinTime.getAttrValues();
            for (int i = 0; i < set.size(); i++) {
                Object object = set.getObjectAt(i);
                if (object instanceof ASN1UTCTime) {
                    ASN1UTCTime asn1Time = (ASN1UTCTime) object;
                    System.out.println("Firmato il: " + asn1Time.getDate());
                } else if (object instanceof DERUTCTime) {
                    DERUTCTime derTime = (DERUTCTime) object;
                    System.out.println("Firmato il: " + derTime.getDate());
                }
            }
        }
    }

    private void risali(X500Name issuer) {
        System.out.print(issuer);
    }

    private void detailCertificate(X509Certificate cert) throws CertificateEncodingException {

        System.out.println("Dal:" + cert.getNotBefore());
        System.out.println("Al:" + cert.getNotAfter());
        boolean valid = true;
        try {
            cert.checkValidity();
        } catch (CertificateExpiredException e) {
            System.out.println("Scaduto");
            valid = false;
        } catch (CertificateNotYetValidException e) {
            System.out.println("Non ancora valido");
            valid = false;
        }

        if (valid) {
            System.out.println("Certificato temporalmente valido");
        }

        //X500Principal iss = cert.getIssuerX500Principal();
        //X500Principal sub = cert.getSubjectX500Principal();
        //String issName = iss.getName("RFC2253");
        //String subName = sub.getName("RFC2253");
        //X500Name x500nameIss = new JcaX509CertificateHolder(cert).getIssuer();
        //X500Name x500nameSub = new JcaX509CertificateHolder(cert).getSubject();
        //RDN[] a = x500nameIss.getRDNs();
        //RDN[] b = x500nameSub.getRDNs();
        //RDN cn = x500nameSub.getRDNs(BCStyle.CN)[0];
        //IETFUtils.valueToString(cn.getFirst().getValue());
        //System.out.println("Dati firmatario: " + x500nameSub);
        //System.out.println("Dati Emittente: " + x500nameIss);
        X500Principal certActual = cert.getSubjectX500Principal();
        System.out.println("Attuale: " + certActual);

        X500Principal certFirm = cert.getIssuerX500Principal();
        System.out.println("Superiore: " + certFirm);

    }

    private void inspectAllCert(Store store) throws CertificateException {
        //recupero tutto con un selettore nullo
        ArrayList<X509CertificateHolder> a = new ArrayList(store.getMatches(null));
        for (X509CertificateHolder x509CertificateHolder : a) {
            X509Certificate cert = new JcaX509CertificateConverter().setProvider("BC").getCertificate(x509CertificateHolder);
            System.out.println(cert);
        }
    }

    private LinkedList<LinkedList<X509Certificate>> orderAllCert(Store store) throws CertificateException, NoSuchAlgorithmException, NoSuchProviderException {

        LinkedList<LinkedList<X509Certificate>> listListCert = new LinkedList();

        //recupero tutto con un selettore nullo
        ArrayList<X509CertificateHolder> listCertDatFirm = new ArrayList(store.getMatches(null));
        Iterator<X509CertificateHolder> itListCertDatFirm = listCertDatFirm.iterator();

        //Ciclo 1 per i self signed
        while (itListCertDatFirm.hasNext()) {
            X509CertificateHolder x509CertificateHolder = itListCertDatFirm.next();
            X509Certificate cert = new JcaX509CertificateConverter().setProvider("BC").getCertificate(x509CertificateHolder);
            if (SignRelationshipHelper.isSelfSigned(cert)) {
                // lo aggiungo in una nuova lista
                LinkedList<X509Certificate> newListCert = new LinkedList<>();
                newListCert.add(cert);
                listListCert.add(newListCert);
                //lo rimuovo dalla vecchia
                itListCertDatFirm.remove();
            }
        }

        //Ciclo 2 che itera fino a che ci sono cambiamenti
        boolean changed = true;
        while (changed) {
            changed = false;
            itListCertDatFirm = listCertDatFirm.iterator();
            while (itListCertDatFirm.hasNext()) {
                X509CertificateHolder x509CertificateHolder = itListCertDatFirm.next();
                X509Certificate cert = new JcaX509CertificateConverter().setProvider("BC").getCertificate(x509CertificateHolder);

                //scorro tutte le liste
                int indexListListCert = 0;
                int indexListCert = 0;
                for (LinkedList<X509Certificate> listCert : listListCert) {
                    for (X509Certificate x509Certificate : listCert) {
                        if (SignRelationshipHelper.isSigneable(cert, x509Certificate)) {
                            //System.out.println(cert);
                            //System.out.println("FIRMATO");
                            //System.out.println(x509Certificate);

                            indexListCert = listCert.indexOf(x509Certificate);
                            indexListListCert = listListCert.indexOf(listCert);
                            changed = true;
                        }
                        if (changed) {
                            break;
                        }
                    }
                    if (changed) {
                        break;
                    }
                }

                if (changed) {
                    LinkedList<X509Certificate> listCert = listListCert.get(indexListListCert);
                    listCert.add(indexListCert, cert);
                    itListCertDatFirm.remove();
                }

            }
        }

        //ciclo 3 i restanti certificati sono...
        return listListCert;
    }

}
